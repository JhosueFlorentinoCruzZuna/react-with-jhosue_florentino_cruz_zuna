import Image from "next/image";
import styles from "./page.module.css";
import Logo from "@/componets/Logo";
import Seeker from "@/componets/Seeker";
import Icon from "@/componets/Icon";
import Font from "@/componets/Font";
import Title from "@/componets/Title";
import Button from "@/componets/Button";
import Info from "@/componets/Info";
import Tit from "@/componets/Tit";
import ActionAreaCard from "@/componets/Comp";
import Box from '@mui/material/Box';
import For from "@/componets/For";
import FolderList from "@/componets/Fo";
import List from "@/componets/List";

export default function Home() {
  return (
    <main className={styles.main}>
      <header>
        <nav>
        <div>
          <Logo title="NEXTON" title2="eCommerce"></Logo>
        </div>
        <div>
          <Seeker title="Search in products..." image="/image/buscador.svg"></Seeker>
        </div>
        <div className="hero_icon">
          <Icon name="/image/Icon_1.svg"></Icon>
          <Icon name="/image/icon_cart.svg"></Icon>
        </div>
        </nav>
        <section className="secOne" >
          <div className="textOne">
            <Title parrafo="Starting from: $49.99" title="Exclusive collection" tiltle2="for everyone"></Title>
            <Button title="Explore now" ></Button>
          </div>
          <div className="punto">
            <p>.</p>
            <p>.</p>
            <p>.</p>
          </div>
        </section>
      </header>
      <section className="sec2">
        <div className="bor">
          <Info img="/image/carrito.svg" title="Free shipping" title2="On orders over $50.00" icon></Info>
            <Image src="/image/barra.svg" width={100} height={100} alt="" />
          <Info img="/image/carrito.svg" title="Free shipping" title2="On orders over $50.00" icon></Info>
          <Image src="/image/barra.svg" width={100} height={100} alt="" />
          <Info img="/image/carrito.svg" title="Free shipping" title2="On orders over $50.00" icon></Info>
          <Image src="/image/barra.svg" width={100} height={100} alt=""/>
          <Info img="/image/carrito.svg" title="Free shipping" title2="On orders over $50.00" icon></Info>
        </div>
      </section>
      <section className="sec3">
        <Tit title="Start exploring." title2="Good things are waiting for you"></Tit>
        <div>
        <For title="For Men s" title2="Starting at $24"></For>
        <For title="For Men s" title2="Starting at $24"></For>
        <For title="For Men s" title2="Starting at $24"></For>
        </div>
      </section>
      <section>
        <Tit title="Recommendations." title2="Best matching products for you"></Tit>
        <Box className="reloj">
        <ActionAreaCard></ActionAreaCard>
        <ActionAreaCard></ActionAreaCard>
        <ActionAreaCard></ActionAreaCard>
        <ActionAreaCard></ActionAreaCard>
        </Box>
      </section>
      <footer>
        <Box className="foo">
        <FolderList></FolderList>
        <List title="Getting started" text="Release Notes" text2="Upgrade Guide" text3="Browser Support" text4="Dark Mode" ></List>
        <List title="Getting started" text="Release Notes" text2="Upgrade Guide" text3="Browser Support" text4="Dark Mode"></List>
        <List title="Getting started" text="Release Notes" text2="Upgrade Guide" text3="Browser Support" text4="Dark Mode"></List>
        </Box>
        
      </footer>
    </main>
  );
}
