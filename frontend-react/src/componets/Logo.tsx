export default function Logo(props:{title:string,title2:string}){
    return(
        <div className="logo">
            <h2>{props.title}</h2>
            <h5>{props.title2}</h5>
        </div>
    )
}