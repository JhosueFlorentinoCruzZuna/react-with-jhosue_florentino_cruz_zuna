import Image from "next/image"
export default function Icon(props:{name:string}){
    return(
        <div>
                    <Image src={props.name} width={35} height={100} alt="" />
        </div>
    )
}