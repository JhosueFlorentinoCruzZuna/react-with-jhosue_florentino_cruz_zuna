import Image from "next/image"
export default function Seeker(props:{title:string,image:string}){
    return(
        <div className="bus">
            <Image src={props.image} width={100} height={20} alt="" />
            <input type="search"  placeholder={props.title} className="buscador" />
        </div>
    )
}