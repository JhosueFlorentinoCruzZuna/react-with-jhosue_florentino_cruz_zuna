'use client';
import Image from "next/image"
import Info from "./Info"
export default function For(props:{title:string,title2:string}){
    return(
        <div className="box1">
            <div>
            <Info img="/image/barra.svg" title={props.title} title2={props.title2}  ></Info>
            </div>
            <div>
            <Image src="/image/barra.svg" width={100} height={80} alt="" />
            <h5>Shop Now</h5>
            </div>
        </div>
    )
}
