import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import ImageIcon from '@mui/icons-material/Image';
import WorkIcon from '@mui/icons-material/Work';
import BeachAccessIcon from '@mui/icons-material/BeachAccess';
import Logo from './Logo';

export default function FolderList() {
  return (
    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper', margin: '20px 0' }}>
      <Logo title="NEXTON" title2="eCommerce"></Logo>
      <ListItem>
        <ListItemAvatar>
          <Avatar src="/image/facebook.svg" >
            <ImageIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Facebook"  />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar src="/image/youtube.svg">
            <ImageIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Youtube"  />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar src="/image/telegram.svg">
            <ImageIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Telegram"  />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar src="/image/tewtter.svg">
            <ImageIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="PhotoTwitter"  />
      </ListItem>
    </List>
  );
}
