import Image from "next/image"
export default function Info(props:{img:string,title:string,title2:string,icon?:boolean}){
    return(
        props.icon?
        <div className="inf">
            <div>
            <Image src={props.img} width={40} height={100} alt=""/>
            </div>
            <div className="a">
            <h4>{props.title}</h4>
            <h5>{props.title2}</h5> 
            </div>
        </div>
        :
        <div className="inf">
            <div className="a">
            <h4>{props.title}</h4>
            <h5>{props.title2}</h5>
            </div>
        </div>
    )
}